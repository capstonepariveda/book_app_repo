﻿using CapstoneBookApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace CapstoneBookApp.DataAccessLayer
{
    public class BookAppContext : DbContext
    {
      
            public BookAppContext() : base("BookAppContext")
            {

            }

            public DbSet<Book> Books { get; set; }
            public DbSet<Library> Libraries { get; set; }
            public DbSet<Copy> Copies { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}