﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CapstoneBookApp.DataAccessLayer;
using CapstoneBookApp.Models;

namespace CapstoneBookApp.Controllers
{
    public class CopiesController : Controller
    {
        private BookAppContext db = new BookAppContext();

        // GET: Copies
        public ActionResult Index()
        {
            var copies = db.Copies.Include(c => c.Book).Include(c => c.Library);
            return View(copies.ToList());
        }

        // GET: Copies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Copy copy = db.Copies.Find(id);
            if (copy == null)
            {
                return HttpNotFound();
            }
            return View(copy);
        }

        // GET: Copies/Create
        public ActionResult Create()
        {
            ViewBag.BookID = new SelectList(db.Books, "BookID", "Title");
            ViewBag.LibraryID = new SelectList(db.Libraries, "LibraryID", "LibraryName");
            return View();
        }

        // POST: Copies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CopyID,CheckedOut,BookFormat,BookID,LibraryID")] Copy copy)
        {
            if (ModelState.IsValid)
            {
                db.Copies.Add(copy);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BookID = new SelectList(db.Books, "BookID", "Title", copy.BookID);
            ViewBag.LibraryID = new SelectList(db.Libraries, "LibraryID", "LibraryName", copy.LibraryID);
            return View(copy);
        }

        // GET: Copies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Copy copy = db.Copies.Find(id);
            if (copy == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookID = new SelectList(db.Books, "BookID", "Title", copy.BookID);
            ViewBag.LibraryID = new SelectList(db.Libraries, "LibraryID", "LibraryName", copy.LibraryID);
            return View(copy);
        }

        // POST: Copies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CopyID,CheckedOut,BookFormat,BookID,LibraryID")] Copy copy)
        {
            if (ModelState.IsValid)
            {
                db.Entry(copy).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookID = new SelectList(db.Books, "BookID", "Title", copy.BookID);
            ViewBag.LibraryID = new SelectList(db.Libraries, "LibraryID", "LibraryName", copy.LibraryID);
            return View(copy);
        }

        // GET: Copies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Copy copy = db.Copies.Find(id);
            if (copy == null)
            {
                return HttpNotFound();
            }
            return View(copy);
        }

        // POST: Copies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Copy copy = db.Copies.Find(id);
            db.Copies.Remove(copy);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
