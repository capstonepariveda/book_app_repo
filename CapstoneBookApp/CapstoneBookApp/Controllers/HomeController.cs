﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CapstoneBookApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Books()
        {
            ViewBag.Message = "Books page.";

            return View();
        }

        public ActionResult Copies()
        {
            ViewBag.Message = "Copies page.";

            return View();
        }

        public ActionResult Libraries()
        {
            ViewBag.Message = "Libraries page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}