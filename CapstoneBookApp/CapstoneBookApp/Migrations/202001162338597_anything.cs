﻿namespace CapstoneBookApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class anything : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Book",
                c => new
                    {
                        BookID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Author = c.String(),
                        Pages = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        Published = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.BookID);
            
            CreateTable(
                "dbo.Copy",
                c => new
                    {
                        CopyID = c.Int(nullable: false, identity: true),
                        CheckedOut = c.Boolean(nullable: false),
                        BookFormat = c.Int(),
                        BookID = c.Int(nullable: false),
                        LibraryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CopyID)
                .ForeignKey("dbo.Book", t => t.BookID, cascadeDelete: true)
                .ForeignKey("dbo.Library", t => t.LibraryID, cascadeDelete: true)
                .Index(t => t.BookID)
                .Index(t => t.LibraryID);
            
            CreateTable(
                "dbo.Library",
                c => new
                    {
                        LibraryID = c.Int(nullable: false, identity: true),
                        LibraryName = c.String(),
                        Manager = c.String(),
                    })
                .PrimaryKey(t => t.LibraryID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Copy", "LibraryID", "dbo.Library");
            DropForeignKey("dbo.Copy", "BookID", "dbo.Book");
            DropIndex("dbo.Copy", new[] { "LibraryID" });
            DropIndex("dbo.Copy", new[] { "BookID" });
            DropTable("dbo.Library");
            DropTable("dbo.Copy");
            DropTable("dbo.Book");
        }
    }
}
